import Calculator from '../src/services/calculator';

describe('calculate', () => {
  const calc = new Calculator();
  it('add', () => {
    const res = calc.sum(5, 2);
    expect(res).toBe(7);
  });

  it('substract', () => {
    const res = calc.difference(5, 2);
    expect(res).toBe(3);
  });

  it('async add', async () => {
    const res = calc.sum(5, 8);
    expect(res).toBe(13), 1000; /*optional timeout*/
  });
});
