export default class Calculator {
  sum(a: number, b: number): number {
    const c = a + b;
    return c;
  }

  difference(a: number, b: number): number {
    const c = a - b;
    return c;
  }
}
